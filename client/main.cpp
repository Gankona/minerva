#include <QGuiApplication>
#include <QtQuick>
#include "C++/maincontrol.h"

int main(int argc, char** argv){
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/Images/icon_bird.png"));

    MainControl *control = new MainControl;
    control->start();

    return app.exec();
}
