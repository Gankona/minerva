#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include <QDir>
#include <QDebug>
#include <QObject>
#include <QQuickView>
#include <QPluginLoader>
#include <QQmlContext>
#include <QQuickItem>
#include <QGuiApplication>
#include <QtCore>
#include <QtGui>

#include "account.h"

#include "datalessonbuffer.h"
#include "server.h"

class MainControl : public QObject
{
    Q_OBJECT
private:
    Server *server;
    QQuickView *view;
    DataLessonBuffer *dataBuffer;

public:
    MainControl();

    void start();
    Q_INVOKABLE void quit();
};

#endif // MAINCONTROL_H
