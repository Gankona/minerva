#include "datalessonbuffer.h"

DataLessonBuffer::DataLessonBuffer(QObject *parent) : QObject(parent)
{

}

void DataLessonBuffer::registerType()
{
    qmlRegisterType<DataLessonBuffer>("Qt_dataBuffer", 1, 0, "LessonDisplayType");
}

void DataLessonBuffer::setDisplayType(LessonDisplayType displayType)
{
    if (m_displayType == displayType)
        return;

    m_displayType = displayType;
    emit displayTypeChanged(displayType);
}

DataLessonBuffer::LessonDisplayType DataLessonBuffer::displayType() const
{
    return m_displayType;
}

void DataLessonBuffer::getLessonInfo(QDate date, int lessonNumber)
{
    qDebug() << date << lessonNumber;
}
