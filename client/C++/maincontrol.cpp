#include "maincontrol.h"

MainControl::MainControl()
{
    Account::registerType();
    M_namespace::registerType();
    DataLessonBuffer::registerType();

    server = new Server(this);
    dataBuffer = new DataLessonBuffer(this);
    view = new QQuickView(QUrl("qrc:/QML/main.qml"));

    view->rootContext()->setContextProperty("Qt_main", this);
    view->rootContext()->setContextProperty("Qt_server", server);
    view->rootContext()->setContextProperty("Qt_databuffer", dataBuffer);
}

void MainControl::start()
{
    view->resize(400, 600);
    view->show();
    //    QMetaObject::invokeMethod(rootObject(), "addNewPersonalCompanion", Q_ARG(QVariant, "1244"));
    //    QMetaObject::invokeMethod(rootObject(), "addNewPersonalCompanion", Q_ARG(QVariant, "drtefegdrghdr"));
    //    QMetaObject::invokeMethod(rootObject(), "addNewPersonalCompanion", Q_ARG(QVariant, "qwdfgdfherhj"));
}

void MainControl::quit()
{
    qApp->quit();
}
