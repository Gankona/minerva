#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QtNetwork/QTcpSocket>
#include <QtCore/QDataStream>
#include <QtCore/QDate>
#include <QtCore/QDebug>

class Server : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *socket;
    bool isConnected;

public:
    explicit Server(QObject *parent = 0);

    // get
    Q_INVOKABLE void getInfoAboutDate(QDate date);
    Q_INVOKABLE void getNextDate(QDate date);
    Q_INVOKABLE void getPrevDate(QDate date);

    // set
    Q_INVOKABLE void setMessage(QString nameCompanion, QString message);
};

#endif // SERVER_H
