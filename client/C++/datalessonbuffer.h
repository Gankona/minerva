#ifndef DATALESSONBUFFER_H
#define DATALESSONBUFFER_H

#include <QtCore>
#include <QtQml>

class DataLessonBuffer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(LessonDisplayType displayType READ displayType WRITE setDisplayType NOTIFY displayTypeChanged)

public:
    DataLessonBuffer(QObject *parent = nullptr);

    enum class LessonDisplayType {
        ByDay = 0,
        ByWeek = 1,
        Cusom = 2,
    };
    Q_ENUMS(LessonDisplayType)

    static void registerType();
    LessonDisplayType displayType() const;
    void setDisplayType(LessonDisplayType displayType);

    Q_INVOKABLE void getLessonInfo(QDate date, int lessonNumber);

private:
    LessonDisplayType m_displayType;

signals:
    void displayTypeChanged(LessonDisplayType displayType);
};

#endif // DATALESSONBUFFER_H
