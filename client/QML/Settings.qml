import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: maincolor

    Text {
        color: maintextcolor
        anchors.fill: parent
        text: "Тут скоро\nбудуть\nналаштування"
        font.pointSize: 32
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}
