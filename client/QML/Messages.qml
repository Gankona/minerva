import QtQuick 2.0

Rectangle {
    anchors.fill: parent

    function addNewPersonalCompanion(name){
        personalMessageModel.append({ "text" : name })
    }

    function addNewGroupCompanion(name){
        groupMessageModel.append({ "text" : name })
    }

    function clearGroupCompanion(){
        groupMessageModel.clear()
    }

    function clearPersonalCompanion(){
        personalMessageModel.clear()
    }

    Rectangle {
        id: tabPersonal
        color: currentcolor
        height: size
        width: parent.width / 2
        anchors.left: parent.left
        anchors.top: parent.top

        Text {
            text: "Особисті"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                tabGroup.color = hidecolor
                tabPersonal.color = currentcolor
                personal.visible = true
                group.visible = false
            }
        }
    }

    Rectangle {
        id: tabGroup
        color: hidecolor
        height: size
        width: parent.width / 2
        anchors.right: parent.right
        anchors.top: parent.top

        Text {
            text: "Групові"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                tabPersonal.color = hidecolor
                parent.color = currentcolor
                group.visible = true
                personal.visible = false
            }
        }
    }

    Rectangle {
        id: personal
        color: maincolor
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: tabGroup.bottom
        visible: true

        ListModel {
            id: personalMessageModel

            ListElement {
                text: "Человек первый"
            }
            ListElement {
                text: "Человек второй"
            }
            ListElement {
                text: "Человек третий"
            }
            ListElement {
                text: "Человек четвертый"
            }
        }

        ListView {
            id: viewPersonalMessage

            anchors.margins: 0
            anchors.fill: parent
            spacing: 2
            model: personalMessageModel
            clip: true

            delegate: Item {
                id: personalMessageDelegate

                property var view: ListView.view
                property var isCurrent: ListView.isCurrentItem

                width: view.width
                height: size

                Rectangle {
                    anchors.fill: parent

                    Text {
                        anchors.fill: parent
                        text: "%1%2".arg(model.text).arg(isCurrent ? " *" : "")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //view.currentIndex = model.index
                            topPanel.showBackButton()
                            topPanel.setNewTitle("Особисте: " + text)
                            hideAll()
                            dialog.visible = true
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: group
        color: maincolor
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: tabGroup.bottom
        visible: false

        ListModel {
            id: groupMessageModel

            ListElement {
                text: "Человек первый"
            }
            ListElement {
                text: "Человек второй"
            }
            ListElement {
                text: "Человек третий"
            }
            ListElement {
                text: "Человек четвертый"
            }
        }

        ListView {
            id: viewGroupMessage

            anchors.margins: 0
            anchors.fill: parent
            spacing: 2
            model: groupMessageModel
            clip: true

            delegate: Item {
                id: groupMessageDelegate

                property var view: ListView.view
                property var isCurrent: ListView.isCurrentItem

                width: view.width
                height: size

                Rectangle {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    width: parent.width - size

                    Text {
                        anchors.fill: parent
                        text: "%1%2".arg(model.text).arg(isCurrent ? " *" : "")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //view.currentIndex = model.index
                            topPanel.showBackButton()
                            topPanel.setNewTitle("Групове: " + text)
                            hideAll()
                            dialog.visible = true
                        }
                    }
                }

                Rectangle {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    width: size

                    Image {
                        anchors.centerIn: parent
                        source: "qrc:/Images/account.png"
                        sourceSize.width: parent.width * 2 / 3
                        sourceSize.height: parent.height * 2 / 3
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            hideAll()
                            topPanel.setNewTitle("учасники " + model.text)
                            topPanel.showBackButton()
                            companionlist.visible = true
                        }
                    }
                }
            }
        }
    }
}
