import QtQuick 2.0
import QtQml 2.2

Rectangle {
    anchors.fill: parent
    color: hidecolor

    function addLesson(number, startLesson, endLesson, warning, title, teacher, value){
        lessonModel.append({   number: numberLesson,
                               startLesson: startTimeLesson,
                               endLesson: endTimeLesson,
                               warning: warningLesson,
                               title: titleLesson,
                               teacher: teacherName,
                               value: valueLesson})
    }

    function updateCurrentDate(date){
        lessonModel.clear()
        currentDate = date
        timeTableTitleText.text = currentDate.toLocaleDateString(Qt.locale("uk_UK"), "dddd yyyy-MM-dd")
    }

    Rectangle {
        id: timeTableTitle
        height: size
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: maincolor

        Rectangle {
            anchors.right: nextDate.left
            anchors.left: prevDate.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            color: maincolor

            Text {
                id: timeTableTitleText
                color: maintextcolor
                anchors.fill: parent
                font.pointSize: 12
                text: currentDate.toLocaleDateString(Qt.locale("uk_UK"), "dddd yyyy-MM-dd")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll()
                    calendar.visible = true
                    topPanel.showBackButton()
                    topPanel.setNewTitle("Календар")
                }
            }
        }

        Rectangle {
            id: prevDate
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: size
            color: maincolor

            Image {
                source: "qrc:/Images/back.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Qt_server.getPrevDate(currentDate)
                    lessonModel.clear()
                }
            }
        }

        Rectangle {
            id: nextDate
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: size
            color: maincolor

            Image {
                source: "qrc:/Images/back.png"
                sourceSize.width: parent.width * 2 / 3
                sourceSize.height: parent.height * 2 / 3
                anchors.centerIn: parent
                mirror: true
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Qt_server.getNextDate(currentDate)
                    lessonModel.clear()
                }
            }
        }
    }

    Rectangle {
        id: timeTabelRect
        anchors.top: timeTableTitle.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: maincolor

        ListModel {
            id: lessonModel

            ListElement {
                numberLesson: "1"
                startTimeLesson: "8:00"
                endTimeLesson: "8:45"
                warningLesson: "!"
                titleLesson: "Rest"
                valueLesson: "3"
                teacherName: "Aleksey"
            }
            ListElement {
                numberLesson: "2"
                startTimeLesson: "8:55"
                endTimeLesson: "9:40"
                warningLesson: "!"
                titleLesson: "Fra"
                valueLesson: "4"
                teacherName: "Nina"
            }
            ListElement {
                numberLesson: "3"
                startTimeLesson: "9:50"
                endTimeLesson: "10:35"
                warningLesson: "!"
                titleLesson: "Eng"
                valueLesson: "-"
                teacherName: "Nikolay"
            }
            ListElement {
                numberLesson: "4"
                startTimeLesson: "10:50"
                endTimeLesson: "11:45"
                warningLesson: "!"
                titleLesson: "Ukr"
                valueLesson: "-"
                teacherName: "Sergey"
            }
        }

        ListView {
            id: viewLesson

            anchors.margins: 0
            anchors.fill: parent
            spacing: 2
            model: lessonModel
            clip: true

            delegate: Item {
                id: lessonDelegate

                property int position
                property var view: ListView.view
                property var isCurrent: ListView.isCurrentItem

                width: view.width
                height: size

                Rectangle {
                    anchors.fill: parent

                    Rectangle {
                        id: numberLessonRect
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        width: size

                        Text {
                            anchors.fill: parent
                            text: numberLesson
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }

                    Rectangle {
                        id: timeLessonRect
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        width: size * 4 / 3

                        Text {
                            anchors.fill: parent
                            text: startTimeLesson + '\n' + endTimeLesson
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }

                    Rectangle {
                        id: warningLessonRect
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: timeLessonRect.left
                        width: size * 2 / 3

                        Text {
                            anchors.fill: parent
                            text: warningLesson
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }

                    Rectangle {
                        id: valueLessonRect
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: warningLessonRect.left
                        width: size * 2 / 3

                        Text {
                            anchors.fill: parent
                            text: valueLesson
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }

                    Rectangle {
                        id: titleLessonRect
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.left: numberLessonRect.right
                        anchors.right: valueLessonRect.left

                        Text {
                            anchors.fill: parent
                            text: titleLesson + '\n' + teacherName
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            hideAll()
                            dayinfo.visible = true
                            topPanel.showBackButton()
                            topPanel.setNewTitle("Інформація про заняття")
                            Qt_databuffer.getLessonInfo(currentDate, numberLesson)
                        }
                    }
                }
            }
        }
    }
}
