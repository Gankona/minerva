import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: maincolor

    Text {
        id: homeText
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/8
        color: "white"
        text: "Minerva"
        font.pointSize: 36
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        color: maincolor
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: homeText.bottom
        anchors.bottom: parent.bottom

        Image {
            source: "qrc:/Images/icon_bird.png"
            anchors.centerIn: parent
        }
    }
}
