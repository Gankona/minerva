import QtQuick 2.5

Rectangle {
    anchors.fill: parent
    color: maincolor

    ListModel {
        id: personalMessageModel

        ListElement {
            text: "Человек 1"
        }
        ListElement {
            text: "Человек 2"
        }
        ListElement {
            text: "Человек 3"
        }
        ListElement {
            text: "Человек 4"
        }
        ListElement {
            text: "Человек 5"
        }
        ListElement {
            text: "Человек 6"
        }
        ListElement {
            text: "Человек 7"
        }
        ListElement {
            text: "Человек 8"
        }
        ListElement {
            text: "Человек 9"
        }
        ListElement {
            text: "Человек 10"
        }
        ListElement {
            text: "Человек 11"
        }
        ListElement {
            text: "Человек 12"
        }
        ListElement {
            text: "Человек 13"
        }
        ListElement {
            text: "Человек 14"
        }
        ListElement {
            text: "Человек 15"
        }
        ListElement {
            text: "Человек 16"
        }
        ListElement {
            text: "Человек 17"
        }
        ListElement {
            text: "Человек 18"
        }
        ListElement {
            text: "Человек 19"
        }
    }

    ListView {
        id: viewPersonalMessage

        anchors.margins: 0
        anchors.fill: parent
        spacing: 2
        model: personalMessageModel
        clip: true

        delegate: Item {
            id: personalMessageDelegate

            property var view: ListView.view
            property var isCurrent: ListView.isCurrentItem

            width: view.width
            height: size

            Rectangle {
                anchors.fill: parent

                Text {
                    anchors.fill: parent
                    text: "%1%2".arg(model.text).arg(isCurrent ? " *" : "")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    anchors.fill: parent
//                    onClicked: {
//                        //view.currentIndex = model.index
//                        menuButton.visible = false
//                        backButton.visible = true
//                        title.text = "Личные: " + text
//                        hideAll()
//                        dialog.visible = true
//                    }
                }
            }
        }
    }
}
