QT += qml quick network
CONFIG += c++14
TARGET = "Minerva"

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    QML/*.qml

DISTFILES += \
    #QML/Settings.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

SOURCES += \
    main.cpp \
    C++/maincontrol.cpp \
    C++/server.cpp \
    C++/accountmanager.cpp \
    C++/datalessonbuffer.cpp

HEADERS += \
    C++/maincontrol.h \
    C++/server.h \
    C++/accountmanager.h \
    C++/datalessonbuffer.h

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../minervaCore/release/ -lminervaCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../minervaCore/debug/ -lminervaCore
else:unix: LIBS += -L$$OUT_PWD/../minervaCore/ -lminervaCore

INCLUDEPATH += $$PWD/../minervaCore
DEPENDPATH += $$PWD/../minervaCore
