#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QObject>
#include <QtCore/QDataStream>
#include "minervacore_global.h"
#include <QtQml>
#include "m_namespace.h"

class MINERVACORESHARED_EXPORT Account : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString login READ login WRITE setlogin NOTIFY loginChanged)
    Q_PROPERTY(M_namespace::AccountType denaid READ denaid WRITE setdenaid NOTIFY denaidChanged)

private:
    QString m_login;

    M_namespace::AccountType m_denaid;

public:
    explicit Account(QObject *parent = 0);

    QString login() const { return m_login; }
    M_namespace::AccountType denaid() const { return m_denaid; }

    static void registerType(){
        qmlRegisterType<Account>("minerva.core.account", 1, 0, "Account");
    }

    friend QDataStream& operator << (QDataStream &s, Account &a)
    {
        return s << a.m_login << a.m_denaid;
    }

    friend QDataStream& operator >> (QDataStream &s, Account &a)
    {
        return s >> a.m_login >> a.m_denaid;
    }

public slots:
    void setlogin(QString login)
    {
        if (m_login == login)
            return;

        m_login = login;
        emit loginChanged(login);
    }

    void setdenaid(M_namespace::AccountType denaid)
    {
        if (m_denaid == denaid)
            return;

        m_denaid = denaid;
        emit denaidChanged(denaid);
    }

signals:
    void loginChanged(QString login);
    void denaidChanged(M_namespace::AccountType denaid);
};

#endif // ACCOUNT_H
