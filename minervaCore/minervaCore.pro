#-------------------------------------------------
#
# Project created by QtCreator 2016-12-01T07:52:56
#
#-------------------------------------------------

QT -= gui
QT += qml

CONFIG += c++14

TARGET = minervaCore
TEMPLATE = lib

DEFINES += MINERVACORE_LIBRARY

SOURCES += minervacore.cpp \
    account.cpp

HEADERS += minervacore.h\
        minervacore_global.h \
    account.h \
    m_namespace.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
