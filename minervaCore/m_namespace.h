#ifndef M_NAMESPACE_H
#define M_NAMESPACE_H

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QMetaEnum>
#include <QtCore/QMetaObject>
#include <QtCore/QObject>
#include <QtQml>

class M_namespace : public QObject
{
    Q_OBJECT
public:
//--Account-type----------------------------------------------------------
    enum class AccountType {
        Guest = 0xE00,
        Student = 0xE01,
        Parent = 0xE02,
        Teacher = 0xE03,
        School = 0xE04,
        Admin = 0xE05,
    };
    Q_ENUM(AccountType)

    friend QDebug operator << (QDebug dbg, AccountType enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }

    friend QDataStream& operator << (QDataStream &s, AccountType &t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, AccountType &t){
        int i;
        s >> i;
        t = static_cast<M_namespace::AccountType>(i);
        return s;
    }
//--end-------------------------------------------------------------------

//--Type-request----------------------------------------------------------
    enum class TypeRequest {
        Add     = 0xF00,
        Get     = 0xF01,
        Set     = 0xF02,
        Change  = 0xF03,
        Remove  = 0xF04,
        Create  = 0xF05,
        Close   = 0xF06,
        Tie     = 0xF07,
    };
    Q_ENUM(TypeRequest)

    friend QDebug operator << (QDebug dbg, TypeRequest enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }

    friend QDataStream& operator << (QDataStream &s, TypeRequest &t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, TypeRequest &t){
        int i;
        s >> i;
        t = static_cast<M_namespace::TypeRequest>(i);
        return s;
    }

//--end-------------------------------------------------------------------

//--Request---------------------------------------------------------------
    enum class Request {
        Grade        = 0x000,   // оценка
        Warning      = 0x001,   // предупреждения
        Notification = 0x002,   // оповещения
        Homework     = 0x003,   // домашняя работа
        Present      = 0x004,   // присутствие
        Message      = 0x005,   // сообщения
        Chat         = 0x006,   // чат
        Contact      = 0x007,   // контакты
        LessonInfo   = 0x008,   // информация про занятие
        Student      = 0x009,   // студент
        OpenAccess   = 0x00A,   // меняет доступ
        School       = 0x00B,   // школа

        // get only
        DateInfo    = 0x0A0,    // информация про день
        WeekInfo    = 0x0A1,    // информация про неделю
        StudentInfo = 0x0A2,    // информация про студента
        ClassInfo   = 0x0A3,    // информация про класс
        SchoolInfo  = 0x0A4,    // информация про школу
    };
    Q_ENUM(Request)

    friend QDebug operator << (QDebug dbg, Request enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }

    friend QDataStream& operator << (QDataStream &s, Request &t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, Request &t){
        int i;
        s >> i;
        t = static_cast<M_namespace::Request>(i);
        return s;
    }
//--end-------------------------------------------------------------------

static void registerType()
{
    qmlRegisterType<M_namespace>("minerva.core.m_namespace", 1, 0, "M_namespace");
    //qmlRegisterUncreatableType<M_namespace::AccountType>("minerva.core.m_namespace", 1, 0, "AccountType", "error");
    //qmlRegisterType<M_namespace::Request    >("minerva.core.m_namespace", 1, 0, "Request");
    //qmlRegisterType<M_namespace::TypeRequest>("minerva.core.m_namespace", 1, 0, "TypeRequest");
}

};


#endif // M_NAMESPACE_H
