QT += network
QT -= gui

CONFIG += c++14

SOURCES += \
    main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../minervaCore/release/ -lminervaCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../minervaCore/debug/ -lminervaCore
else:unix: LIBS += -L$$OUT_PWD/../minervaCore/ -lminervaCore

INCLUDEPATH += $$PWD/../minervaCore
DEPENDPATH += $$PWD/../minervaCore
